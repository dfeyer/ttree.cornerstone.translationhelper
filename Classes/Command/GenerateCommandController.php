<?php

namespace Ttree\Cornerstone\TranslationHelper\Command;

use Neos\ContentRepository\Domain\Service\NodeTypeManager;
use Neos\ContentRepository\Exception\NodeTypeNotFoundException;
use Neos\Flow\Cli\CommandController;
use Neos\Flow\Annotations as Flow;
use Neos\Flow\I18n\Locale;
use Neos\Flow\I18n\Xliff\V12\XliffParser;
use Neos\Flow\Package\Exception\UnknownPackageException;
use Neos\Flow\Package\PackageManager;
use Symfony\Component\Yaml\Parser;
use Symfony\Component\Yaml\Yaml;

class GenerateCommandController extends CommandController
{
    const COLUMN_WIDTH = 30;

    /**
     * @var NodeTypeManager
     * @Flow\Inject
     */
    protected $nodeTypeManager;

    /**
     * @var PackageManager
     * @Flow\Inject
     */
    protected $packageManager;

    /**
     * @var XliffParser
     * @Flow\Inject
     */
    protected $parser;

    /**
     * Create the translations file for the give NodeType in the current package
     *
     * @param string $nodeType The current node type
     * @param string $source The source language, default to 'en'
     * @param string $package The package to store the translation file, if empty, the package name is based on the node type namespace
     */
    public function nodeTypeTranslationsCommand(string $nodeType, string $source = 'en', ?string $package = null)
    {
        $sourceLocale = new Locale($source);

        $this->outputLine();
        $this->outputLine('<b>Generate translation file <info>%s</info></b>', [strtoupper($sourceLocale->getLanguage())]);


        try {
            $nodeType = $this->nodeTypeManager->getNodeType($nodeType);
        } catch (NodeTypeNotFoundException $e) {
            $this->outputFatal('The node type is not defined');
        }

        $this->outputLine($this->paddedLine('✓ Current node type', $nodeType));

        list($nodeTypeNamespace, $nodeTypeShortname) = explode(':', $nodeType);

        if ($package === null) {
            $package = $nodeTypeNamespace;
        }

        $packageName = $package;
        try {
            $package = $this->packageManager->getPackage($package);
        } catch (UnknownPackageException $exception) {
            $this->outputFatal('The package is not defined');
        }

        $this->outputLine($this->paddedLine('✓ Current package', $packageName));

        $configurationFile = $package->getPackagePath() . 'Configuration/NodeTypes.' . $nodeTypeShortname . '.yaml';
        if (!@is_file($configurationFile)) {
            $this->outputFatal('⚠ Missing NodeType configuration (%s)', [basename($configurationFile)]);
        }

        $translationFile = $package->getPackagePath() . 'Resources/Private/Translations/' . $source . '/NodeTypes/' . str_replace('.', '/', $nodeTypeShortname) . '.xlf';
        $this->outputLine($this->paddedLine('✓ XLIFF File', $this->relativePath($translationFile)));

        if (@is_file($translationFile)) {
            $action = 'skip';

            try {
                $this->outputLine('<comment>⚠ Translation file exists</comment>');
                $message = '  Choose an action           : <b>r</b>eplace|<b>u</b>pdate|[<b>s</b>kip] ';
                $this->output->askAndValidate($message, function (string $response) use (&$action) {
                    $response = trim($response);
                    static $validAnswers = ['r' => 'replace', 'replace' => 'replace', 'u' => 'update', 'update' => 'update', 's' => 'skip', 'skip' => 'skip'];
                    if (!in_array($response, array_keys($validAnswers))) {
                        throw new \InvalidArgumentException(' ⚠ Unsupported action: ' . $response, 1535033728982);
                    }
                    $action = $validAnswers[$response];
                    return true;
                }, null, $action);
            } catch (\Exception $e) {
                $this->outputFatal('  ⚠ Invalid action: %s', [$exception->getMessage()]);
            }

            if ($action === 'skip') {
                $this->outputFatal('⚠ Processing skipped');
            }

            $this->outputLine($this->paddedLine('✓ Selected action', $action));
        } else {
            $action = 'create';
        }

        $configuration = Yaml::parseFile($configurationFile);
        $translatablePath = $this->i18nStringFinder($configuration[$nodeType->getName()]);
        $this->outputLine($this->paddedLine('✓ Number of i18n string in YAML', count($translatablePath)));

        $data = $this->parser->getParsedData($translationFile);

        \Neos\Flow\var_dump($translatablePath);
        \Neos\Flow\var_dump($data);
        if ($data[0]['sourceLocale'] === $sourceLocale) {

        }

    }

    protected function i18nStringFinder(array $configuration, ?string $path = null, array &$results = [])
    {
        foreach ($configuration as $segment => $value) {
            $currentPath = $path ? $path . '.' . $segment : $segment;
            if (is_string($value) && $value === 'i18n') {
                $results[$currentPath] = $currentPath;
            }
            if (is_array($value)) {
                $this->i18nStringFinder($value, $currentPath, $results);
            }
        }

        return array_values($results);
    }
    
    protected function relativePath(string $path): string
    {
        return str_replace(FLOW_PATH_ROOT, '', $path);
    }

    protected function outputFatal(string $text, array $arguments = [], int $exitCode = 1)
    {
        $this->outputLine();
        $this->outputLine('<error>' . $text . '</error>', $arguments);
        $this->quit($exitCode);
    }

    protected function paddedErrorLine(string $label, string $value) {
        return vsprintf('<error>%s</error> : %s', [str_pad($label, self::COLUMN_WIDTH, ' '), $value]);
    }

    protected function paddedInfoLine(string $label, string $value) {
        return vsprintf('<error>%s</error> : %s', [str_pad($label, self::COLUMN_WIDTH, ' '), $value]);
    }

    protected function paddedLine(string $label, string $value) {
        return vsprintf('%s : %s', [str_pad($label, self::COLUMN_WIDTH, ' '), $value]);
    }
}
